package fr.iutinfo.events;

public class JoinedEventDto {
	
	private int user;
	private int event;
	
	
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public int getEvent() {
		return event;
	}
	public void setEvent(int event) {
		this.event = event;
	}
	
	

}
